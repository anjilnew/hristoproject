import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import MyFirstComponent from '@/components/MyFirstComponent'
import Editor from '@/components/Editor'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/my-first',
      name: 'first_tab',
      component: MyFirstComponent,
      props: (route) => ({
        query: route.query.q,
        ord: route.query.ord,
        ord_by: route.query.ord_by,
        page: route.query.page,
        per_page: route.query.per_page
      }
      )
    },
    {
      path: '/edit/:model',
      name: 'edit',
      component: Editor,
      props: (route) => ({
        id: route.query.id,
        model: route.params.mode
      })
    }
  ]
})
